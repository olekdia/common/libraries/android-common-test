package com.olekdia.androidcommontest

import android.view.MotionEvent
import android.view.View
import androidx.test.espresso.DataInteraction
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.MotionEvents
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import org.hamcrest.Matcher

fun DataInteraction.touchDown(x: Float, y: Float, eventHolder: EventHolder) {
    perform(
        object : ViewAction {
            override fun getConstraints(): Matcher<View> {
                return isDisplayed()
            }

            override fun getDescription(): String {
                return "Send touch events."
            }

            override fun perform(uiController: UiController, view: View) {
                // Get view absolute position
                val location = IntArray(2)
                view.getLocationOnScreen(location)

                // Offset coordinates by view position
                val coordinates = floatArrayOf(x + location[0], y + location[1])
                val precision = floatArrayOf(1F, 1F)

                // Send down event, pause, and send up
                eventHolder.event = MotionEvents.sendDown(uiController, coordinates, precision).down
            }
        }
    )
}

fun ViewInteraction.touchDown(x: Float, y: Float, eventHolder: EventHolder) {
    perform(
        object : ViewAction {
            override fun getConstraints(): Matcher<View> {
                return isDisplayed()
            }

            override fun getDescription(): String {
                return "Send touch events."
            }

            override fun perform(uiController: UiController, view: View) {
                // Get view absolute position
                val location = IntArray(2)
                view.getLocationOnScreen(location)

                // Offset coordinates by view position
                val coordinates = floatArrayOf(x + location[0], y + location[1])
                val precision = floatArrayOf(1F, 1F)

                // Send down event, pause, and send up
                eventHolder.event = MotionEvents.sendDown(uiController, coordinates, precision).down
            }
        }
    )
}


fun ViewInteraction.touchUp(eventHolder: EventHolder) {
    perform(
        object : ViewAction {
            override fun getConstraints(): Matcher<View> {
                return isDisplayed()
            }

            override fun getDescription(): String {
                return "Send touch events."
            }

            override fun perform(uiController: UiController, view: View) {
                MotionEvents.sendUp(uiController, eventHolder.event)
            }
        }
    )
}


fun ViewInteraction.touchMove(x: Float, y: Float, eventHolder: EventHolder) {
    perform(
        object : ViewAction {
            override fun getConstraints(): Matcher<View> {
                return isDisplayed()
            }

            override fun getDescription(): String {
                return "Send touch events."
            }

            override fun perform(uiController: UiController, view: View) {
                // Get view absolute position
                val location = IntArray(2)
                view.getLocationOnScreen(location)

                // Offset coordinates by view position
                val coordinates = floatArrayOf(x + location[0], y + location[1])
                MotionEvents.sendMovement(uiController, eventHolder.event, coordinates)
            }
        }
    )
}

class EventHolder {
    @JvmField
    var event: MotionEvent? = null
}