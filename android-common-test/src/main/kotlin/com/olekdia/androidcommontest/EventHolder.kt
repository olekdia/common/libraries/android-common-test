package com.olekdia.androidcommontest

import android.view.MotionEvent

class EventHolder {
    @JvmField
    var event: MotionEvent? = null
}