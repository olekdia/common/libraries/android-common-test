package com.olekdia.androidcommontest

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.media.AudioManager
import android.os.Build
import android.text.TextUtils
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.annotation.RequiresApi
import androidx.test.espresso.Espresso
import androidx.test.espresso.Root
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiDevice
import org.hamcrest.Description
import org.hamcrest.Matcher
import org.hamcrest.Matchers
import org.hamcrest.TypeSafeMatcher
import org.junit.Assert

//--------------------------------------------------------------------------------------------------
//  Delay
//--------------------------------------------------------------------------------------------------

fun delayLookOnUI() {
    delayCustom(1500)
}

fun delayMediumToResume() {
    delayCustom(300)
}

fun delayShortToResume() {
    delayCustom(100)
}

fun delayCustom(delayMillis: Long) {
    try {
        Thread.sleep(delayMillis)
    } catch (e: InterruptedException) {
        e.printStackTrace()
    }
}

//--------------------------------------------------------------------------------------------------
//  Assert
//--------------------------------------------------------------------------------------------------

fun assertTextShown(text: String) {
    Espresso
        .onView(ViewMatchers.withText(text))
        .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
}

fun assertSnackbarShown(id: Int, text: String) {
    Espresso
        .onView(Matchers.allOf(ViewMatchers.withId(id), ViewMatchers.withText(text)))
        .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
}

fun assertSnackbarNotShown(id: Int, text: String) {
    Espresso
        .onView(Matchers.allOf(ViewMatchers.withId(id), ViewMatchers.withText(text)))
        .check(ViewAssertions.doesNotExist())
}

fun assertToastShown(text: String) {
    Espresso
        .onView(ViewMatchers.withText(text))
        .inRoot(ToastMatcher())
        .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
}

fun assertToastNotShown(text: String) {
    Espresso
        .onView(ViewMatchers.withText(text))
        .inRoot(ToastMatcher())
        .check(ViewAssertions.matches(Matchers.not(ViewMatchers.isDisplayed())))
}

@RequiresApi(Build.VERSION_CODES.M)
fun assertSoundsAudible() {
    val am = InstrumentationRegistry.getInstrumentation()
        .context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
    Assert.assertEquals(am.isMusicActive, true)
    Assert.assertEquals(am.isStreamMute(AudioManager.STREAM_MUSIC), false)
}

@RequiresApi(Build.VERSION_CODES.M)
fun assertSoundsMute() {
    val am = InstrumentationRegistry.getInstrumentation()
        .context.getSystemService(Context.AUDIO_SERVICE) as AudioManager
    Assert.assertEquals(am.isMusicActive, false)
    Assert.assertEquals(am.isStreamMute(AudioManager.STREAM_MUSIC), true)
}

class ToastMatcher : TypeSafeMatcher<Root>() {

    override fun describeTo(description: Description) {
        description.appendText("is toast")
    }

    public override fun matchesSafely(root: Root): Boolean {
        val type = root.windowLayoutParams.get().type
        if (type == WindowManager.LayoutParams.TYPE_TOAST) {
            val windowToken = root.decorView.windowToken
            val appToken = root.decorView.applicationWindowToken
            if (windowToken === appToken) {
                // windowToken == appToken means this window isn't contained by any other windows.
                // if it was a window for an activity, it would have TYPE_BASE_APPLICATION.
                return true
            }
        }
        return false
    }
}

//--------------------------------------------------------------------------------------------------
//  Matcher methods
//--------------------------------------------------------------------------------------------------

fun childAtPosition(parentMatcher: Matcher<View>, position: Int): Matcher<View> {

    return object : TypeSafeMatcher<View>() {
        override fun describeTo(description: Description) {
            description.appendText("Child at position $position in parent ")
            parentMatcher.describeTo(description)
        }

        public override fun matchesSafely(view: View): Boolean {
            val parent = view.parent
            return parent is ViewGroup
                    && parentMatcher.matches(parent)
                    && view == parent.getChildAt(position)
        }
    }
}

//--------------------------------------------------------------------------------------------------
// Work with app
//--------------------------------------------------------------------------------------------------

fun setThisAppDefault(intent: Intent, text: String, applicationId: CharSequence) {
    delayShortToResume()
    if (!isThisAppDefault(intent, applicationId)) {
        val device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
        val appItem = device.findObject(By.text(text))
        appItem?.click()
        appItem?.click()

        val alwaysBtn = device.findObject(By.text("JUST ONCE")) ?: device.findObject(By.text("Just once"))

        alwaysBtn?.click()

        delayMediumToResume()
    }
}

private fun isThisAppDefault(intent: Intent, applicationId: CharSequence): Boolean {
    val resolveInfo = InstrumentationRegistry.getInstrumentation().targetContext.packageManager
        .resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY)
    var defaultPkg: String? = null
    if (resolveInfo != null) {
        if (resolveInfo.activityInfo != null) {
            defaultPkg = resolveInfo.activityInfo.packageName
        }
    }
    return TextUtils.equals(applicationId, defaultPkg)
}

//--------------------------------------------------------------------------------------------------
// Work with device
//--------------------------------------------------------------------------------------------------

fun Activity.rotateScreen() {
    val context = InstrumentationRegistry.getInstrumentation().targetContext
    val orientation = context.resources.configuration.orientation

    this.requestedOrientation =
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
        } else {
            ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        }
}

fun pressSystemBack() {
    UiDevice.getInstance(InstrumentationRegistry.getInstrumentation()).pressBack()
}